#include "Student.h"
#include "Code.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

string Student::name = "Students";
long long Student::codePerTick = 1;
long long Student::amount = 0;
long long Student::price = 10;
long long Student::upgrade = 1;
long long Student::multiplier = 1;
int Student::id = 1;



void Student::click() {

		Code::Add(codePerTick*amount);
}

void Student::show() {

	int width = 17;

	cout << left << setw(2) << setfill(' ')  << " " << id	<< "| ";
	cout << left << setw(width) << setfill(' ') << name << "| ";
	cout << left << setw(width) << setfill(' ') << amount      << "| ";
	cout << left << setw(width) << setfill(' ') << codePerTick << "| ";
	cout << left << setw(width) << setfill(' ') << upgrade		<< "| ";
	cout << left << setw(width) << setfill(' ') << upgradeCost() << "| ";
	cout << left << setw(width) << setfill(' ') << price;
	cout << endl;

}

long long Student::upgradeCost() {

	return (upgrade*price);

}

void Student::buy(){

	if (price <= Code::returnCount())
	{
		Code::Add(-price);
		price += multiplier;
		amount++;
	}
}

void Student::upgradeBuy(){

	if (upgradeCost() <= Code::returnCount())
	{
		Code::Add(-upgradeCost());
		upgrade++;
		codePerTick += multiplier;
	}

}

void Student::save(ofstream& saveFile) {

	saveFile << codePerTick << "," << amount << "," << price << "," << upgrade << "\n";

}

void Student::load(ifstream& loadFile) {

	string cPT;
	string am;
	string prc;
	string up;

	getline(loadFile, cPT, ',');
	getline(loadFile, am, ',');
	getline(loadFile, prc, ',');
	getline(loadFile, up, '\n');

	codePerTick = stoi(cPT);
	amount = stoi(am);
	price = stoi(prc);
	upgrade = stoi(up);
}