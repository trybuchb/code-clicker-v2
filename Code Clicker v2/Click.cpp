#include "Click.h"
#include "Code.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

long long	 Click::codePerTick = 1;
long long	 Click::upgrade = 1;
int Click::id = 0;

void Click::click() {

	Code::Add(codePerTick);

}

void Click::show() {

	int width = 17;

	cout << left << setw(2) << setfill(' ') << " " << id << "| ";
	cout << left << setw(width) << setfill(' ') << "Click" << "| ";
	cout << left << setw(width) << setfill(' ') << " " << "| ";
	cout << left << setw(width) << setfill(' ') << codePerTick << "| ";
	cout << left << setw(width) << setfill(' ') << upgrade << "| ";
	cout << left << setw(width) << setfill(' ') << Click::upgradeCost() << "| ";
	cout << left << setw(width) << setfill(' ') << " ";
	cout << endl;
}

long long Click::upgradeCost() {

	return (upgrade*100);

}

void Click::upgradeBuy() {

	if (Click::upgradeCost() <= Code::returnCount())
	{
		Code::Add(-Click::upgradeCost());
		Click::upgrade++;
		Click::codePerTick += 10;
	}
}

void Click::save(ofstream& saveFile) {

	saveFile << codePerTick << "," << upgrade << "\n";

}

void Click::load(ifstream& loadFile) {

	string cPT;
	string up;

	getline(loadFile, cPT, ',');
	getline(loadFile, up, '\n');

	codePerTick = stoi(cPT);
	upgrade = stoi(up);
}