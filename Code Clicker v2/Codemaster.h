#pragma once
#include <fstream>

using namespace std;

class Codemaster
{
	friend class Shop;

	static string name;
	static long long codePerTick;
	static long long amount;
	static long long price;
	static long long upgrade;
	static long long multiplier;
	static int id;

public:
	static void click();
	static void show();
	static long long	 upgradeCost();
	static void buy();
	static void upgradeBuy();
	static void save(ofstream& saveFile);
	static void load(ifstream& loadFile);
};