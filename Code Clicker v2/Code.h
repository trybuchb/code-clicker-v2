#pragma once
class Code
{
	friend class Shop;
	friend class Save;
	static long long count;

public:
	static void Show();
	static void Add(long long amount);
	static long long returnCount();

};