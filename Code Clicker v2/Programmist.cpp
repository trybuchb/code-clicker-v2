#include "Programmist.h"
#include "Code.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

string Programmist::name = "Programmist";
long long Programmist::codePerTick = 36;
long long Programmist::amount = 0;
long long Programmist::price = 180;
long long Programmist::upgrade = 1;
long long Programmist::multiplier = 45;
int Programmist::id = 3;


void Programmist::click() {

	Code::Add(codePerTick*amount);
}

void Programmist::show() {

	int width = 17;

	cout << left << setw(2) << setfill(' ') << " " << id << "| ";
	cout << left << setw(width) << setfill(' ') << name << "| ";
	cout << left << setw(width) << setfill(' ') << amount << "| ";
	cout << left << setw(width) << setfill(' ') << codePerTick << "| ";
	cout << left << setw(width) << setfill(' ') << upgrade << "| ";
	cout << left << setw(width) << setfill(' ') << upgradeCost() << "| ";
	cout << left << setw(width) << setfill(' ') << price;
	cout << endl;

}

long long Programmist::upgradeCost() {

	return (upgrade*price);

}

void Programmist::buy() {

	if (price <= Code::returnCount())
	{
		Code::Add(-price);
		price += multiplier;
		amount++;
	}
}

void Programmist::upgradeBuy() {

	if (upgradeCost() <= Code::returnCount())
	{
		Code::Add(-upgradeCost());
		upgrade++;
		codePerTick += multiplier;
	}

}

void Programmist::save(ofstream& saveFile) {

	saveFile << codePerTick << "," << amount << "," << price << "," << upgrade << "\n";

}

void Programmist::load(ifstream& loadFile) {

	string cPT;
	string am;
	string prc;
	string up;

	getline(loadFile, cPT, ',');
	getline(loadFile, am, ',');
	getline(loadFile, prc, ',');
	getline(loadFile, up, '\n');

	codePerTick = stoi(cPT);
	amount = stoi(am);
	price = stoi(prc);
	upgrade = stoi(up);
}