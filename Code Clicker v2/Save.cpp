#include "Save.h"
#include "Student.h"
#include "Click.h"
#include "Code.h"
#include "Teacher.h"
#include "Programmist.h"
#include "Codemaster.h"
#include "Ai.h"

#include <fstream>
#include <iostream>

using namespace std;

string Save::fileName = "save.csv";

bool Save::save() {

	ofstream saveFile;
	saveFile.open(fileName);

	saveFile << Code::returnCount() << "\n";
	Click::save(saveFile);
	Student::save(saveFile);
	Teacher::save(saveFile);
	Programmist::save(saveFile);
	Codemaster::save(saveFile);
	Ai::save(saveFile);

	saveFile.close();
	return true;
}


bool Save::load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	string countStr;
	getline(loadFile, countStr, '\n');
	Code::count = stoi(countStr);

	Click::load(loadFile);
	Student::load(loadFile);
	Teacher::load(loadFile);
	Programmist::load(loadFile);
	Codemaster::load(loadFile);
	Ai::load(loadFile);

	loadFile.close();
	return true;
}