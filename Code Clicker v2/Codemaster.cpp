#include "Codemaster.h"
#include "Code.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

string Codemaster::name = "Codemaster";
long long Codemaster::codePerTick = 140;
long long Codemaster::amount = 0;
long long Codemaster::price = 1300;
long long Codemaster::upgrade = 1;
long long Codemaster::multiplier = 360;
int Codemaster::id = 4;


void Codemaster::click() {

	Code::Add(codePerTick*amount);
}

void Codemaster::show() {

	int width = 17;

	cout << left << setw(2) << setfill(' ') << " " << id << "| ";
	cout << left << setw(width) << setfill(' ') << name << "| ";
	cout << left << setw(width) << setfill(' ') << amount << "| ";
	cout << left << setw(width) << setfill(' ') << codePerTick << "| ";
	cout << left << setw(width) << setfill(' ') << upgrade << "| ";
	cout << left << setw(width) << setfill(' ') << upgradeCost() << "| ";
	cout << left << setw(width) << setfill(' ') << price;
	cout << endl;

}

long long Codemaster::upgradeCost() {

	return (upgrade*price);

}

void Codemaster::buy() {

	if (price <= Code::returnCount())
	{
		Code::Add(-price);
		price += multiplier;
		amount++;
	}
}

void Codemaster::upgradeBuy() {

	if (upgradeCost() <= Code::returnCount())
	{
		Code::Add(-upgradeCost());
		upgrade++;
		codePerTick += multiplier;
	}

}

void Codemaster::save(ofstream& saveFile) {

	saveFile << codePerTick << "," << amount << "," << price << "," << upgrade << "\n";

}

void Codemaster::load(ifstream& loadFile) {

	string cPT;
	string am;
	string prc;
	string up;

	getline(loadFile, cPT, ',');
	getline(loadFile, am, ',');
	getline(loadFile, prc, ',');
	getline(loadFile, up, '\n');

	codePerTick = stoi(cPT);
	amount = stoi(am);
	price = stoi(prc);
	upgrade = stoi(up);
}