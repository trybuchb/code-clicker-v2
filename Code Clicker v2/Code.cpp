#include "Code.h"
#include <iostream>
#include <windows.h>

using namespace std;

long long Code::count = 0;

void Code::Show() {

	cout << " Code: " << count;
	Sleep(100);
	cout << "\r";

}

void Code::Add(long long amount) {

	count += amount;

}

long long Code::returnCount() {

	return Code::count;
}

