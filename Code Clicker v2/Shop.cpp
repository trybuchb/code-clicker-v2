#include <windows.h>
#include <conio.h>
#include <iostream>
#include <string>
#include <iomanip>


#include "Click.h"
#include "Shop.h"
#include "Student.h"
#include "Code.h"
#include "Teacher.h"
#include "Programmist.h"
#include "Codemaster.h"
#include "Ai.h"

using namespace std;

void Shop::print() {

	bool showFlag = false;
	bool buyFlag = false;
	bool upgradeFlag = false;
	system("cls");

	while (true)
	{
		Sleep(100);
		while (_kbhit()) _getch();

		if (buyFlag == true)
		{

			if (GetAsyncKeyState(49)) //1
			{
				showFlag = false;
				Student::buy();
			}

			if (GetAsyncKeyState(50)) //2
			{
				showFlag = false;
				Teacher::buy();
			}

			if (GetAsyncKeyState(51)) //3
			{
				showFlag = false;
				Programmist::buy();
			}

			if (GetAsyncKeyState(52)) //4
			{
				showFlag = false;
				Codemaster::buy();
			}

			if (GetAsyncKeyState(53)) //5
			{
				showFlag = false;
				Ai::buy();
			}

		}

		if (upgradeFlag == true)
		{

			if (GetAsyncKeyState(48)) //0
			{
				showFlag = false;
				Click::upgradeBuy();
			}

			if (GetAsyncKeyState(49)) //1
			{
				showFlag = false;
				Student::upgradeBuy();
			}

			if (GetAsyncKeyState(50)) //2
			{
				showFlag = false;
				Teacher::upgradeBuy();
			}

			if (GetAsyncKeyState(51)) //3
			{
				showFlag = false;
				Programmist::upgradeBuy();
			}

			if (GetAsyncKeyState(52)) //4
			{
				showFlag = false;
				Codemaster::upgradeBuy();
			}

			if (GetAsyncKeyState(53)) //5
			{
				showFlag = false;
				Ai::upgradeBuy();
			}

		}

		if (GetAsyncKeyState(85))  //U
		{
			upgradeFlag = true;
			buyFlag = false;

			showFlag = false;
		}



		if (GetAsyncKeyState(66))  //B
		{	
			buyFlag = true;
			upgradeFlag = false;

			showFlag = false;
		}

		if (GetAsyncKeyState(90)) //Z
		{
			buyFlag = false;
			upgradeFlag = false;
			showFlag = false;
		}


		if (showFlag == false)
		{
			system("cls");
			Shop::show();
			cout << "------------------------------------------------------SHOP------------------------------------------------------" << endl;
			cout << "------ Buy: B ------ Upgrade: U ------ Back: Q -----------------------------------------------------------------" << endl;
			cout << "Code: " << Code::count << endl;
			if (buyFlag == true)
			{
				cout << "Id to buy: " << endl;
			}
			if (upgradeFlag == true)
			{
				cout << "Id to upgrade: " << endl;
			}


			showFlag = true;
		}

		if (GetAsyncKeyState(81))  //Q
		{
			upgradeFlag = false;
			buyFlag = false;

			break;
		}

	}




}

void Shop::show() {

	int width = 19;
	cout << left << setw(3) << setfill(' ') << " id";
	cout << left << setw(width) << setfill(' ') << "| Type:";
	cout << left << setw(width) << setfill(' ') << "| Amount:";
	cout << left << setw(width) << setfill(' ') << "| Code per Tick:";
	cout << left << setw(width) << setfill(' ') << "| Upgrade level:";
	cout << left << setw(width) << setfill(' ') << "| Upgrade price:";
	cout << left << setw(width) << setfill(' ') << "| Buy price:";
	cout << endl;
	cout << left << setw(112) << setfill('-') << "";
	cout << endl;

	Click::show();
	Student::show();
	Teacher::show();
	Programmist::show();
	Codemaster::show();
	Ai::show();

	
}

void Shop::click() {

	Student::click();
	Teacher::click();
	Programmist::click();
	Codemaster::click();
	Ai::click();
}