#include <string>
#include <iostream>
#include <windows.h>
#include <conio.h>
#include <fstream>

#include "Code.h"
#include "Click.h"
#include "Student.h"
#include "Shop.h"
#include "Save.h"

using namespace std;

struct my_numpunct : numpunct<char> {
	string do_grouping() const { return "\03"; }
};


int main() {

	locale loc(cout.getloc(), new my_numpunct);
	cout.imbue(loc);

	cout << "Load game: L" << endl;
	cout << "New game: N" << endl;

	while (true)
	{
		if (GetAsyncKeyState(78))  //N
		{
			break;
		}

		if (GetAsyncKeyState(76))  //L
		{
			Save::load();
			break;
		}
	}

	system("cls");
	bool showFlag = false;

	while (true) {

		Sleep(100);
		while (_kbhit()) _getch();
		std::cin.clear();
		std::cin.sync();

		if (GetAsyncKeyState(32))  //spacebar
		{
			Click::click();

		}

		if (GetAsyncKeyState(83))  //S
		{
			Shop::print();
			system("cls");
			showFlag = false;

		}
		
		if (GetAsyncKeyState(27))  //ESC
		{
			system("cls");
			cout << " Quit: Y" << endl;
			cout << " Return: N" << endl;
			while (true)
			{
				if (GetAsyncKeyState(89))  //Y
				{
					if (Save::save()) {

						cout << " Progress saved" << endl;
						Sleep(1500);

					}
					return 0;
				}
				if (GetAsyncKeyState(78))  //N
				{
					system("cls");
					showFlag = false;
					break;
				}
			}
		}

		if (showFlag == false)
		{
			Shop::show();
			cout << "------ Shop: S ------ Exit: ESC ------ Click: SPACE ------------------------------------------------------------" << endl;
			showFlag = true;
		}

		Shop::click();
		Code::Show();

	}

}