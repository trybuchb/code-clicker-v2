#include "Ai.h"
#include "Code.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

string Ai::name = "Ai";
long long Ai::codePerTick = 800;
long long Ai::amount = 0;
long long Ai::price = 10000;
long long Ai::upgrade = 1;
long long Ai::multiplier = 400;
int Ai::id = 5;


void Ai::click() {

	Code::Add(codePerTick*amount);
}

void Ai::show() {

	int width = 17;

	cout << left << setw(2) << setfill(' ') << " " << id << "| ";
	cout << left << setw(width) << setfill(' ') << name << "| ";
	cout << left << setw(width) << setfill(' ') << amount << "| ";
	cout << left << setw(width) << setfill(' ') << codePerTick<< "| ";
	cout << left << setw(width) << setfill(' ') << upgrade << "| ";
	cout << left << setw(width) << setfill(' ') << upgradeCost() << "| ";
	cout << left << setw(width) << setfill(' ') << price;
	cout << endl;

}

long long Ai::upgradeCost() {

	return (upgrade*price);

}

void Ai::buy() {

	if (price <= Code::returnCount())
	{
		Code::Add(-price);
		price += multiplier;
		amount++;
	}
}

void Ai::upgradeBuy() {

	if (upgradeCost() <= Code::returnCount())
	{
		Code::Add(-upgradeCost());
		upgrade++;
		codePerTick += multiplier;
	}

}

void Ai::save(ofstream& saveFile) {

	saveFile << codePerTick << "," << amount << "," << price << "," << upgrade << "\n";

}

void Ai::load(ifstream& loadFile) {

	string cPT;
	string am;
	string prc;
	string up;

	getline(loadFile, cPT, ',');
	getline(loadFile, am, ',');
	getline(loadFile, prc, ',');
	getline(loadFile, up, '\n');

	codePerTick = stoi(cPT);
	amount = stoi(am);
	price = stoi(prc);
	upgrade = stoi(up);
}