#include "Teacher.h"
#include "Code.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

string Teacher::name = "Teacher";
long long Teacher::codePerTick = 7;
long long Teacher::amount = 0;
long long Teacher::price = 50;
long long Teacher::upgrade = 1;
long long Teacher::multiplier = 12;
int Teacher::id = 2;


void Teacher::click() {

	Code::Add(codePerTick*amount);
}

void Teacher::show() {

	int width = 17;

	cout << left << setw(2) << setfill(' ') << " " << id << "| ";
	cout << left << setw(width) << setfill(' ') << name << "| ";
	cout << left << setw(width) << setfill(' ') << amount << "| ";
	cout << left << setw(width) << setfill(' ') << codePerTick << "| ";
	cout << left << setw(width) << setfill(' ') << upgrade << "| ";
	cout << left << setw(width) << setfill(' ') << upgradeCost() << "| ";
	cout << left << setw(width) << setfill(' ') << price;
	cout << endl;

}

long long Teacher::upgradeCost() {

	return (upgrade*price);

}

void Teacher::buy() {

	if (price <= Code::returnCount())
	{
		Code::Add(-price);
		price += multiplier;
		amount++;
	}
}

void Teacher::upgradeBuy() {

	if (upgradeCost() <= Code::returnCount())
	{
		Code::Add(-upgradeCost());
		upgrade++;
		codePerTick += multiplier;
	}

}

void Teacher::save(ofstream& saveFile) {

	saveFile << codePerTick << "," << amount << "," << price << "," << upgrade << "\n";

}

void Teacher::load(ifstream& loadFile) {

	string cPT;
	string am;
	string prc;
	string up;

	getline(loadFile, cPT, ',');
	getline(loadFile, am, ',');
	getline(loadFile, prc, ',');
	getline(loadFile, up, '\n');

	codePerTick = stoi(cPT);
	amount = stoi(am);
	price = stoi(prc);
	upgrade = stoi(up);
}