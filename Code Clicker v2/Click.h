#pragma once
#include <fstream>

using namespace std;

class Click
{
	friend class Shop;

	static long long	 codePerTick;
	static long long	 upgrade;
	static int id;

public:
	static void click();
	static void show();
	static long long upgradeCost();
	static void upgradeBuy();
	static void save(ofstream& saveFile);
	static void load(ifstream& loadFile);
};